import React from 'react';
import './App.css';

const isOperator = /[*/+-]/;
const endsWithOperator = /[*+-/]$/;
const endsWithNegativeSign = /[*/+]-$/;

const NumButton = (props) => (
  <button
    className="button"
    id={props.id}
    value={props.value}
    onClick={props.click}
  >
    {props.value}
  </button>
);

const ClearButton = (props) => (
  <button className="button" id="clear" onClick={props.clear}>
    C
  </button>
);

const DecButton = (props) => (
  <button className="button" id="decimal" onClick={props.click}>
    .
  </button>
);

const EqButton = (props) => (
  <button className="button" id="equals" onClick={props.click}>
    =
  </button>
);

const OperationButton = (props) => (
  <button
    className="button"
    id={props.id}
    value={props.value}
    onClick={props.click}
  >
    {props.value}
  </button>
);

const Display = (props) => (
  <div className="display" id="display">
    {props.value}
  </div>
);

const Formula = (props) => <div className="formula">{props.value}</div>;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentValue: '0',
      prevVal: '0',
      formula: '',
      currentSign: 'pos',
      lastClicked: '',
    };
  }

  handleNumbers(e) {
    if (this.state.currentValue.length < 10) {
      const { currentValue, formula, evaluated } = this.state;
      const value = e.target.value;

      this.setState({ evaluated: false });

      if (evaluated) {
        this.setState({
          currentValue: value,
          formula: value !== '0' ? value : '',
        });
      } else {
        this.setState({
          currentValue:
            currentValue === '0' || isOperator.test(currentValue)
              ? value
              : currentValue + value,
          formula:
            currentValue === '0' && value === '0'
              ? formula === ''
                ? value
                : formula
              : /([^.0-9]0|^0)$/.test(formula)
              ? formula.slice(0, -1) + value
              : formula + value,
        });
      }
    }
  }

  handleOperators(e) {
    const value = e.target.value;
    const { formula, prevVal, evaluated } = this.state;

    this.setState({ currentValue: value, evaluated: false });

    if (evaluated) {
      this.setState({ formula: prevVal + value });
    } else if (!endsWithOperator.test(formula)) {
      this.setState({
        prevVal: formula,
        formula: formula + value,
      });
    } else if (!endsWithNegativeSign.test(formula)) {
      this.setState({
        formula:
          (endsWithNegativeSign.test(formula + value) ? formula : prevVal) +
          value,
      });
    } else if (value !== '‑') {
      this.setState({
        formula: prevVal + value,
      });
    }
  }

  handleEvaluate(e) {
    let expression = this.state.formula;

    while (endsWithOperator.test(expression)) {
      expression = expression.slice(0, -1);
    }

    let answer = Math.round(10000000000 * eval(expression)) / 10000000000;

    this.setState({
      currentValue: answer.toString(),
      formula: expression + '=' + answer,
      prevVal: answer,
      evaluated: true,
    });
  }

  handleDecimal(e) {
    if (this.state.evaluated === true) {
      this.setState({
        currentValue: '0.',
        formula: '0.',
        evaluated: false,
      });
    } else if (
      !this.state.currentValue.includes('.') &&
      this.state.currentValue.length < 10
    ) {
      this.setState({ evaluated: false });
      if (
        endsWithOperator.test(this.state.formula) ||
        (this.state.currentValue === '0' && this.state.formula === '')
      ) {
        this.setState({
          currentValue: '0.',
          formula: this.state.formula + '0.',
        });
      } else {
        this.setState({
          currentValue: this.state.formula.match(/(-?\d+\.?\d*)$/)[0] + '.',
          formula: this.state.formula + '.',
        });
      }
    }
  }

  clear() {
    this.setState({
      currentValue: '0',
      prevVal: '0',
      formula: '',
      currentSign: 'pos',
      lastClicked: '',
    });
  }

  render() {
    const numKeys = [
      { id: 'zero', value: '0' },
      { id: 'one', value: '1' },
      { id: 'two', value: '2' },
      { id: 'three', value: '3' },
      { id: 'four', value: '4' },
      { id: 'five', value: '5' },
      { id: 'six', value: '6' },
      { id: 'seven', value: '7' },
      { id: 'eight', value: '8' },
      { id: 'nine', value: '9' },
    ];

    const operationKeys = [
      { id: 'divide', value: '/' },
      { id: 'multiply', value: '*' },
      { id: 'subtract', value: '-' },
      { id: 'add', value: '+' },
    ];

    return (
      <div className="container">
        <Formula value={this.state.formula} />
        <Display value={this.state.currentValue} />
        <ClearButton clear={() => this.clear()} />
        {operationKeys.map((opKey) => {
          return (
            <OperationButton
              key={opKey.id}
              id={opKey.id}
              value={opKey.value}
              click={(e) => this.handleOperators(e)}
            />
          );
        })}
        {numKeys.reverse().map((numKey) => {
          return (
            <NumButton
              key={numKey.id}
              id={numKey.id}
              value={numKey.value}
              click={(e) => this.handleNumbers(e)}
            />
          );
        })}
        <DecButton click={(e) => this.handleDecimal(e)} />
        <EqButton click={(e) => this.handleEvaluate(e)} />
      </div>
    );
  }
}

export default App;
